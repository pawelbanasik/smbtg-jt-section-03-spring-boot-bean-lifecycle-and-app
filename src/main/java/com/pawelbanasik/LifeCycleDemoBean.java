package com.pawelbanasik;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class LifeCycleDemoBean
		implements InitializingBean, DisposableBean, BeanNameAware, BeanFactoryAware, ApplicationContextAware {

	public LifeCycleDemoBean() {
		System.out.println("## I'm in the LifeCycleBean Constructor");
	}

	// callback method
	@Override
	public void destroy() throws Exception {
		System.out.println("## The Lifecycle bean has been terminated");

	}

	// callback method
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("## The LifeCycleBean has its properties set!");

	}

	// aware interfaces
	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		System.out.println("## Bean Factory has been set");
	}

	// aware interfaces
	@Override
	public void setBeanName(String name) {
		System.out.println("## My Bean Name is: " + name);

	}

	// aware interfaces
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("## Application context has been set");
	}

	// piszemy i dajemy anotacje
	@PostConstruct
	public void postConstruct() {
		System.out.println("## The Post Construct annotated method has been called");
	}

	// piszemy i dajemy anotacje
	@PreDestroy
	public void preDestroy() {
		System.out.println("## The Predestroy annotated method has been called");
	}

	// ta metode trzeba napisac i wywolac
	public void beforeInit() {
		System.out.println("## - Before Init - Called by Bean Post Processor");
	}

	// ta metode trzeba napisac i wywolac
	public void afterInit() {
		System.out.println("## - After init called by Bean Post Processor");
	}
}
