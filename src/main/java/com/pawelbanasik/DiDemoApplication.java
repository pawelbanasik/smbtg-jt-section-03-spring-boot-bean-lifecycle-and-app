package com.pawelbanasik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.pawelbanasik.controller.ConstructorInjectedController;

@SpringBootApplication
public class DiDemoApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(DiDemoApplication.class, args);
		ConstructorInjectedController controller = (ConstructorInjectedController) context
				.getBean("constructorInjectedController");
		System.out.println(controller.hello());
	}
}
