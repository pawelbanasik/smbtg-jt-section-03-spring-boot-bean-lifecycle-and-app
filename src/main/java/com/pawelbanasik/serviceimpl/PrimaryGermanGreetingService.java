package com.pawelbanasik.serviceimpl;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.pawelbanasik.service.GreetingService;

@Primary
@Profile("de")
@Service
public class PrimaryGermanGreetingService implements GreetingService {

	@Override
	public String sayGreeting() {
		return "Primarer Grusdienst";
	}

}
