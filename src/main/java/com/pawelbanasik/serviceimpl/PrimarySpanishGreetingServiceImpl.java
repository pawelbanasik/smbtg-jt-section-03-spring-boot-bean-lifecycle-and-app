package com.pawelbanasik.serviceimpl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.pawelbanasik.service.GreetingService;

@Service
@Profile("es")
public class PrimarySpanishGreetingServiceImpl implements GreetingService {

	@Override
	public String sayGreeting() {
		return "Servicio de Saludo Primario";
	}

}
