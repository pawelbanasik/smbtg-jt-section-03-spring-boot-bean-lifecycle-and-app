package com.pawelbanasik.serviceimpl;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.pawelbanasik.service.GreetingService;

@Service
@Primary
@Profile({ "en", "default" })
public class PrimaryEnglishGreetingServiceImpl implements GreetingService {

	@Override
	public String sayGreeting() {
		return "Hello in English";
	}
}
